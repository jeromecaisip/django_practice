from django.shortcuts import redirect, get_object_or_404
from django.views import generic
from .models import Choice, Question
from django.urls import reverse


# Create your views here.


class IndexView(generic.ListView):
    context_object_name = 'questions_list'
    template_name = 'polls/index.html'

    def get_queryset(self):
        return Question.objects.order_by('-pub_date')[:5]


class DetailView(generic.DetailView):
    model = Question
    template_name = 'polls/detail.html'


class ResultView(generic.DetailView):
    model = Question
    template_name = 'polls/results.html'


def vote(request, question_id):
    if request.method == 'GET':
        return redirect(reverse('polls:detail', args=(question_id,)))
    choice = get_object_or_404(Choice, pk=request.POST.get('choice'))
    choice.count = choice.count + 1
    choice.save()
    return redirect(reverse('polls:results', args=(question_id,)))
